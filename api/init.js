import endpoint from '~/assets/api_endpoint'

export default $axios => ({
  updateToken(accessToken) {
    $axios.setToken(accessToken, 'bearer')
    $axios.setHeader('Authorization', `bearer ${accessToken}`)
  },
  getPageMeta(url) {
    return $axios.get(endpoint.urlMeta, { params: { url } })
  },
  checkToken(currency = '') {
    return $axios.$post(endpoint.tokenApi, Object.assign(endpoint.tokenData, currency))
  },
  getCurrency() {
    return $axios.$get(endpoint.currencyList)
  }
})
