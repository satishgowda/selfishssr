import init from './init'
import plp from './plp'
import pdp from './pdp'
import cart from './cart'
import engage from './engage'
import profile from './profile'
import response from './response'

export default $axios => (Object.assign({}, init($axios), engage($axios), pdp($axios), plp($axios), cart($axios), profile($axios), response($axios)))
