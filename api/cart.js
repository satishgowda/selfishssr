import endpoint from '~/assets/api_endpoint'

export default $axios => ({
  cartIdInit() {
    return $axios.$get(endpoint.cartId)
  },
  cartSync(params) {
    return $axios.$get(endpoint.cartSync, { params })
  },
  cartPeek(params) {
    return $axios.$get(endpoint.cartPeek, { params })
  },
  addProduct(product, params) {
    console.log(params)
    return $axios.$put(endpoint.addToCart, product, { params })
  },
  cartDetails(params) {
    return $axios.$get(endpoint.cartDetails, { params })
  },
  removeProduct(data, params) {
    return $axios.$delete(endpoint.removeFromCart, { data, params })
  },
  getAddressList() {
    return $axios.$get(endpoint.addressList)
  },
  getOrderID(data) {
    return $axios.$post(endpoint.checkout, data)
  },
  getPaymentModes(params) {
    return $axios.$get(endpoint.modes, { params })
  },
  initatePayment(data) {
    const url = data.payment_method === 'PAYTM' ? `${endpoint.initate}?platform=web` : endpoint.initate
    return $axios.$post(url, data)
  },
  clear() {
    return $axios.$patch(endpoint.clear)
  }
})
// async addProduct({ commit, state }, data) {
//   try {
//     const response = await this.$externalApi.addProduct(data, { cart_id: state.cartId })
//     const { products } = response.data
//     commit('addProduct', products)
//   } catch (error) {
//     console.log(error)
//   }
