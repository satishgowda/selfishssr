import enpoints from '~/assets/api_endpoint'

export default $axios => ({
  getAllProducts(params) {
    return $axios.$get(enpoints.allProducts, { params })
  }
})
