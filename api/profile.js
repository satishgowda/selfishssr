import endpoints from '~/assets/api_endpoint'

export default $axios => ({
  myOrders() {
    return $axios.$get(endpoints.orderList)
  },
  getOrderDetails(params) {
    return $axios.$get(endpoints.orderdetails, { params })
  },
  CancelReturnQueries(params) {
    return $axios.$get(endpoints.orderissue, { params })
  },
  initiateCancel(cancelObj) {
    return $axios.$post(endpoints.cancel, cancelObj)
  },
  initiateReturn(returnObj) {
    return $axios.$post(endpoints.return, returnObj)
  },
  getAllAddress() {
    return $axios.$get(endpoints.getAddress)
  },
  updatePassword(data) {
    return $axios.$patch(endpoints.changePassword, data)
  },
  getAllCountries() {
    return $axios.$get(endpoints.countrylist)
  },
  getCustDetails() {
    return $axios.$get(endpoints.getCustomerDetails)
  },
  updatePersonaldet(data) {
    return $axios.$patch(endpoints.updatePersonalDetails, data)
  },
  addressDelete(addressId) {
    return $axios.$delete(endpoints.deleteAddress + addressId)
  },
  addressAdd(data) {
    return $axios.$post(endpoints.addAddress, data)
  },
  editAddress(addressId, data) {
    return $axios.$patch(endpoints.editAddress + addressId, data)
  }
})
