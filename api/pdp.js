import endpoint from '~/assets/api_endpoint'

export default $axios => ({
  getProductDetails(params) {
    console.log(params)
    return $axios.$get(endpoint.pdp, { params })
  },
  getSizeChart(param) {
    return $axios.$get(endpoint.sizeChart, { param })
  },
  wishlistProduct(data) {
    return $axios.$post(endpoint.productWishlist, data)
  },
  getSimilarProduct(data) {
    return $axios.$post(endpoint.similarProduct, data)
  },
  getSimilarBrandProduct(data) {
    return $axios.$post(endpoint.similarProduct, data)
  }
})
