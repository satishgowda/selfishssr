import endpoint from '~/assets/api_endpoint'
export default $axios => ({
  orderComplete(data) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': ''
    }
    return $axios.$post(endpoint.orderComplete, data, { headers })
  },
  statusPooling(params) {
    return $axios.$get(endpoint.pooling, { params })
  }
})
