import endpoint from '~/assets/api_endpoint'

export default $axios => ({
  login(data) {
    console.log($axios)
    return $axios.$post(endpoint.login, data)
  },
  register(data) {
    return $axios.$post(endpoint.register, data)
  },
  logout() {
    return $axios.$post(endpoint.logout)
  }
})
