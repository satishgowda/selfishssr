const routes = [
  {
    name: 'main',
    path: '/',
    component: () => import(/* webpackChunkName: 'home' */'~/pages/index.vue').then(m => m.default || m)
  },
  {
    name: 'AllProducts',
    path: '/allproducts',
    component: () => import(/* webpackChunkName: 'plp' */'~/pages/plp/main.vue').then(m => m.default || m)
  },
  {
    name: 'PDP',
    path: '/product/:product_slug',
    component: () => import(/* webpackChunkName: 'pdp' */'~/pages/pdp/main.vue').then(m => m.default || m)
  },
  /* Login Registest a.k.a engage Section */
  {
    name: 'LoginRegister',
    path: '/login-register',
    component: () => import(/* webpackChunkName: 'loginregister' */'~/pages/engage/login-register.vue').then(m => m.default || m)
  },
  {
    name: 'ForgotPassword',
    path: '/forgotPassword',
    component: () => import(/* webpackChunkName: 'forgotpassword' */'~/pages/engage/forgotPassword.vue').then(m => m.default || m)
  },
  /* Cart Sections */
  {
    name: 'CartLogin',
    path: '/checkout/login',
    component: () => import(/* webpackChunkName: 'cartLogin' */'~/pages/cart/login.vue').then(m => m.default || m)
  },
  {
    name: 'CartRegister',
    path: '/checkout/register',
    component: () => import(/* webpackChunkName: 'cartRegister' */'~/pages/cart/register.vue').then(m => m.default || m)
  },
  {
    name: 'CartAddress',
    path: '/checkout/address',
    component: () => import(/* webpackChunkName: 'cartAddress' */'~/pages/cart/address.vue').then(m => m.default || m)
  },
  {
    name: 'NewCartAddress',
    path: '/checkout/newaddress',
    component: () => import(/* webpackChunkName: 'cartAddress' */'~/pages/cart/newAddress.vue').then(m => m.default || m)
  },
  {
    name: 'CartPayment',
    path: '/checkout/payment',
    component: () => import(/* webpackChunkName: 'cartPayment' */'~/pages/cart/payment.vue').then(m => m.default || m)
  },
  /* profile sections */
  {
    name: 'Profile',
    path: '/profile',
    component: () => import(/* webpackChunkName: 'profile' */'~/pages/profile/overview.vue').then(m => m.default || m),
    children: [
      {
        name: 'Overview',
        path: '/overview',
        component: () => import(/* webpackChunkName: 'Overview' */'~/pages/profile/overview.vue').then(m => m.default || m)
      }
    ]
  },
  {
    name: 'addressBook',
    path: '/addressbook',
    component: () => import(/* webpackChunkName: 'addressBook' */'~/pages/profile/addressbook.vue').then(m => m.default || m),
    children: [
    ]
  },
  {
    name: 'AddNewAddress',
    path: '/newAddress',
    component: () => import(/* webpackChunkName: 'newAddresss' */'~/pages/profile/address-component/AddNewAddress.vue').then(m => m.default || m)
  },
  {
    name: 'personalDetails',
    path: '/personal-details',
    component: () => import(/* webpackChunkName: 'PersonalDetails' */'~/pages/profile/personal-details.vue').then(m => m.default || m)
  },
  {
    name: 'MyOrders',
    path: '/myorders',
    component: () => import(/* webpackChunkName: 'myOrder' */'~/pages/profile/Orders.vue').then(m => m.default || m)
  },
  {
    name: 'accessDetails',
    path: '/access-details',
    component: () => import(/* webpackChunkName: 'details' */'~/pages/profile/access-details.vue').then(m => m.default || m)
  },
  {
    name: 'orderSucess',
    path: '/payment/response/success',
    component: () => import(/* webpackChunkName: 'orderSucess' */'~/pages/orderStatus/sucess.vue').then(m => m.default || m)
  },
  {
    name: 'orderFail',
    path: '/payment/response/failure',
    component: () => import(/* webpackChunkName: 'orderFail' */'~/pages/orderStatus/failure.vue').then(m => m.default || m)
  },
  {
    name: 'PaymentReponse',
    path: '/payment/response',
    component: () => import(/* webpackChunkName: 'paymentResponse' */'~/pages/orderStatus/response.vue').then(m => m.default || m)
  },
  /* static pages */
  {
    name: 'FAQ',
    path: '/pages/faq',
    component: () => import(/* webpackChunkName: 'faq' */'~/pages/misc/faq.vue').then(m => m.default || m)
  },
  {
    name: 'ShippingReturn',
    path: '/shippingandreturn',
    component: () => import(/* webpackChunkName: 'faq' */'~/pages/misc/shippingReturn.vue').then(m => m.default || m)
  },
  {
    name: 'TermsCondition',
    path: '/termsandconditions',
    component: () => import(/* webpackChunkName: 'faq' */'~/pages/misc/termsCondition.vue').then(m => m.default || m)
  },
  {
    name: 'PrivacyPolicy',
    path: '/privacypolicy',
    component: () => import(/* webpackChunkName: 'faq' */'~/pages/misc/privacyPolicy.vue').then(m => m.default || m)
  },
  {
    name: 'Contact',
    path: '/contactus',
    component: () => import(/* webpackChunkName: 'contact' */'~/pages/misc/contact.vue').then(m => m.default || m)
  },
  {
    name: 'AboutUs',
    path: '/aboutus',
    component: () => import(/* webpackChunkName: 'contact' */'~/pages/misc/about.vue').then(m => m.default || m)
  },
  {
    name: 'LookBook',
    path: '/lookbook',
    component: () => import(/* webpackChunkName: 'contact' */'~/pages/misc/lookbook.vue').then(m => m.default || m)
  }

]

export default routes
