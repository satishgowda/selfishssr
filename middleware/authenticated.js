export default ({ store, redirect, req }) => {
  if (!store.state.loginStatus) {
    return redirect('/login-register')
  }
}
