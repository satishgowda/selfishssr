export default ({ store, redirect, req, route }) => {
  if (!store.state.loginStatus) {
    console.log(route.path)
    return redirect(`/login-register?referer=${route.path}`)
  }
}
