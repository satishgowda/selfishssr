export default async ({ route, store, redirect, app }) => {
  try {
    const result = await app.$externalApi.getPageMeta(route.path)
    const response = result.data.data
    if (response.action && response.action.type === 'redirect') {
      redirect(response.action.url)
    }
    store.dispatch('page/updatePageMeta', response)
  } catch (error) {
    console.log(error)
  }
}
/* route middleware util function */
