export default ({ store, redirect, req, route }) => {
  if (!store.state.loginStatus) {
    return redirect(`/login-register?referer=${route.path}`)
  } else if (!store.state.cart.selectedAddress) {
    return redirect('/checkout/address')
  } else if (!store.state.cart.cartProducts.length) {
    return redirect('/checkout/empty')
  }
}
