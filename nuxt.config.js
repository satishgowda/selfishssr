const webpack = require('webpack')
const pkg = require('./package')

const customConfig = process.env.MODE === 'production' ? require('./customConfig/config.production') : process.env.MODE === 'staging' ? require('./customConfig/config.staging') : require('./customConfig/config.local')
const currency = JSON.stringify([
  {
    altCode: '₹',
    text: 'INR'
  },
  {
    altCode: '$',
    text: 'USD'
  },
  {
    altCode: '€',
    text: 'EUR'
  },
  {
    altCode: '£',
    text: 'GBP'
  },
  {
    altCode: '£',
    text: 'GBP'
  },
  {
    altCode: '¥',
    text: 'JPY'
  }
])

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#000' },
  /* router config */
  router: {
    middleware: ['route']
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/main.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/api',
    '~/plugins/auth',
    { src: '~/plugins/vuecarousel', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/router',
    // Doc: https://bootstrap-vue.js.org/docs/
    // 'bootstrap-vue/nuxt',
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    'nuxt-device-detect',
    'cookie-universal-nuxt'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: 'http://service.selfishindia.com'
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    },
    plugins: [
      new webpack.DefinePlugin(Object.assign({}, customConfig, { 'process.currency': currency }))
    ]
  }
}
