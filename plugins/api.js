import api from '../api'

export default (ctx, inject) => {
  inject('externalApi', api(ctx.$axios))
}
