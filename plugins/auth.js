export default ({ store, app: { $axios } }) => {
  $axios.setToken(store.state.authToken, 'bearer')
}
