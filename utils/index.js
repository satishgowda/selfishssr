/* all this utils functions list here */
function validateName(val) {
  const lengthMsg = 'Name should be atleast 5 character'
  const charMsg = 'Name should contain only Alpha characters'
  return !/[\w\s]{5}/i.test(val.trim()) ? lengthMsg : !/^[A-Z\s]+$/i.test(val.trim()) ? charMsg : null
}
function validateEmail(val) {
  const emailMsg = 'Provide a proper Email ID'
  const emailRegex = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
  return !emailRegex.test(val) ? emailMsg : null
}
function validatePassword(val) {
  const passMsg = 'Password must be at least 6 characters'
  return !/^(?=.*?[A-Za-z]).{6,}$/.test(val) ? passMsg : null
}

function getImageFromObject(obj, ext, i, size) {
  // i as 0 if first image is required
  // dont pass size if original big image is required
  let url = ''
  if (obj) {
    const baseUrl = size ? obj.cdnCacheUrl : obj.cdnUrl
    const extension = obj.ext[ext]
    const imgSize = size ? obj.sizes[size] : ''
    const imagePath = obj.imagePath
    const image = i ? obj.images[i] : obj.images[i]
    url = `${baseUrl}${imagePath}${image}${imgSize}${extension}`
  }
  return url
}

export { validateName, validateEmail, validatePassword, getImageFromObject }
