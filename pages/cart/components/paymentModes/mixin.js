const mixin = {
  props: {
    modeData: {
      type: Object,
      default() {
        return {}
      }
    },
    selected: {
      type: Boolean,
      default() {
        return false
      }
    }
  }
}
export default mixin
