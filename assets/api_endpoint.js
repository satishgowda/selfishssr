export default {
  tokenData: {
    app_version: '',
    device_token: 'web',
    device_type: 'web',
    os_version_code: 'web'
  },
  tokenApi: '/auth/token',
  login: 'auth/login',
  logout: 'auth/logout',
  register: 'auth/sign_up',
  cartPeek: '/cart/v1/peek',
  pdp: 'core/v3/product/detail',
  sizeChart: '/core/v2/product/sizechart',
  productWishlist: '/profile/v2/wishlist/customer',
  similarProduct: '/core/v2/category/similar_products',
  similarBrandProduct: '/core/v2/brand/similar_products',
  /* allProducts: '/core/v2/brand/products?brand_id=707', */
  allProducts: '/core/v2/brand/products?brand_id=946',
  urlMeta: '/core/v1/url_meta',
  cartId: '/cart/v1/initialize',
  addToCart: 'cart/v1/add',
  orderList: '/api/order',
  orderdetails: '/api/order/details',
  orderissue: '/api/order/issues',
  cancel: '/api/order/cancel',
  return: 'api/vendor_order/return',
  getAddress: '/profile/v1/customer/address',
  updatePersonalDetails: '/profile/v1/customer',
  getCustomerDetails: '/profile/v1/customer',
  changePassword: '/profile/v1/customer/password',
  countrylist: '/core/country/list',
  deleteAddress: '/profile/v1/customer/address/',
  editAddress: '/profile/v1/customer/address/',
  addAddress: '/profile/v1/customer/address',
  cartSync: '/cart/v1/sync',
  cartDetails: '/cart/v1/detail',
  removeFromCart: '/cart/v1/remove',
  addressList: '/profile/v1/customer/address',
  currencyList: '/core/currency/list',
  initate: '/api/payment/initiate',
  checkout: '/cart/v1/checkout',
  modes: '/api/payment/modes',
  clearCart: '/cart/v1/clear',
  orderComplete: '/api/payment/complete',
  pooling: '/api/payment/status'
}

/* redirect: {
    payment_success: "http://selfish.life/payment/response/success",
    payment_failed: "http://selfish.life/payment/response/failed"

    Gaurang Pansare [4:44 PM]
http://selfish-service.lightbuzz.in/core/v2/product/sizechart?product_id=183150&brand_id=946&category_id=108
http://selfish-service.lightbuzz.in/core/v1/newsletter/subscribed
} */
