/* function getCurrencyObj(currString) {
  return process.currency.find(curr => curr.text === currString)
} */

export const state = () => ({
  loginStatus: false,
  authToken: '',
  cartId: null,
  customerDetails: {},
  currency: {
    altCode: '₹',
    text: 'INR'
  },
  currencyList: [],
  currencyToggle: false
})

export const mutations = {
  toogleCurrencyOption(state) {
    state.currencyToggle = !state.currencyToggle
  },
  saveUserState(state, data) {
    state.loginStatus = data.is_login
    state.authToken = data.access_token
    state.currency = state.currencyList.find(currency => currency.currency_code === data.currency)
  },
  saveUserData(state, data) {
    state.loginStatus = true
    state.authToken = data.auth_token
    state.customerDetails = data.customer_details
    state.currency = state.currencyList.find(currency => currency.currency_code === data.customer_details.currency)
  },
  saveCurrencyList(state, data) {
    state.currencyList = data
  },
  setToken(state, data) {
    state.authToken = data
  },
  saveCartId(state, newCartId) {
    state.cartId = newCartId
  },
  updateCurrency(state, currency) {
    state.currency = currency
  }
}

export const actions = {
  async nuxtServerInit({ commit, dispatch, state }, { app }) {
    try {
      await dispatch('getToken')
      const currencyCheck = app.$externalApi.getCurrency()
      const tokenCheck = app.$externalApi.checkToken()
      const [currencyResponse, tokenResponse] = await Promise.all([currencyCheck, tokenCheck])
      console.log(currencyResponse)
      console.log(tokenResponse)
      commit('saveCurrencyList', currencyResponse.data)
      await dispatch('onTokenInit', tokenResponse.data)
      await dispatch('cart/cartInitSync')
    } catch (error) {
      console.log(error)
    }
  },
  getToken({ commit }) {
    const auth = this.$cookies.get('selfishTkn')
    console.log('Auth token')
    console.log(auth)
    this.$externalApi.updateToken(auth)
    commit('setToken', auth)
  },
  onTokenInit({ commit }, data) {
    this.$externalApi.updateToken(data.access_token)
    this.$cookies.set('selfishTkn', data.access_token, { domain: process.authDomain, path: '/' })
    commit('saveUserState', data)
  },
  onLoginSuccess({ commit }, data) {
    this.$externalApi.updateToken(data.auth_token)
    this.$cookies.set('selfishTkn', data.auth_token, { domain: process.authDomain, path: '/' })
    commit('saveUserData', data)
  },
  toogleCurrencyOption({ commit }) {
    commit('toogleCurrencyOption')
  },
  async updateCurrency({ commit, dispatch }, currency) {
    try {
      dispatch('toogleCurrencyOption')
      commit('updateCurrency', currency)
      const response = await this.$externalApi.checkToken({ currency })
      dispatch('onTokenInit', response.data)
      dispatch('cart/peekProduct')
    } catch (error) {
      console.log(error.response)
    }
  },
  async getCurrencyObj({ commit, dispatch }) {
    try {
      const response = await this.$externalApi.getCurrency()
      console.log(response)
    } catch (error) {
      console.log(error)
    }
  }
}

/* all the utils function will come here */
