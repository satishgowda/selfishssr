export const state = () => ({
  menuToggle: false,
  isModal: false
})

export const mutations = {
  toogleMenu(state, boolValue) {
    state.menuToggle = boolValue
  },
  changeModalStatus(state, boolValue) {
    state.isModal = boolValue
  }
}

export const actions = {
  toogleMenu({ commit }, boolValue) {
    commit('toogleMenu', boolValue)
  },
  changeModalStatus({ commit }, boolValue) {
    commit('changeModalStatus', boolValue)
  }
}
