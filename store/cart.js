/* eslint camelcase: ["error",  {ignoreDestructuring: true}] */
/* eslint camelcase: ["error", {allow: ["cart_id"]}]  */
const erroLists = ['CurrencyMismatchError', 'ProductStockValidation', 'AddressValidation', 'ValidationError', 'RequiredError', 'NotFoundError', 'IntegrityError', 'UnknownError']
function checkForErrorType(errors) {
  return !!errors.find((error) => {
    const errorType = error.error_type
    return erroLists.find(errorname => (errorname === errorType))
  })
}
/* function getDefaultAddress(addressList) {
  const defaultAddress = addressList.find(address => address.is_default)
  return defaultAddress ? defaultAddress.id : null
} */
export const state = () => ({
  bagProducts: [],
  cartId: null,
  cartProducts: [],
  cartDetails: [],
  toProceed: false,
  cartErrors: null,
  couponSelected: null,
  addressList: [],
  selectedAddress: null,
  orderID: null,
  paymentMode: {},
  customerData: {}
})
export const mutations = {
  addProduct(state, products) {
    state.bagProducts = products
  },
  saveCartId(state, newCartId) {
    state.cartId = newCartId
  },
  updateBag(state, products) {
    state.bagProducts = products
  },
  onCartDetail(state, data) {
    state.cartProducts = data.products
    state.cartDetails = data.amount
    state.toProceed = !checkForErrorType(data.errors)
    state.cartErrors = data.errors[0]
  },
  onCartDetailError(state, errors) {
    state.toProceed = false
    state.cartErrors = errors[0]
  },
  onAddressList(state, addressList) {
    state.addressList = addressList
  },
  onAddressSelect(state, addressID) {
    state.selectedAddress = addressID
  },
  onOrderID(state, orderID) {
    state.orderID = orderID
  },
  onPaymentModes(state, data) {
    state.paymentMode = data.payment_modes
    state.customerData = data.customer
  }
}
export const actions = {
  async addProduct({ commit, state }, data) {
    try {
      const response = await this.$externalApi.addProduct(data, { cart_id: state.cartId })
      const detailResponse = await this.$externalApi.cartDetails({ cart_id: state.cartId })
      const { products } = response.data
      const detailData = detailResponse.data
      commit('addProduct', products)
      commit('onCartDetail', detailData)
      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error)
    }
  },
  /*
    this action sync the cart on login
  */
  async cartSync({ commit, dispatch, state }) {
    try {
      const cartId = this.$cookies.get('selfishCrtId')
      const response = await this.$externalApi.cartSync({ cart_id: cartId })
      const { cart_id, products } = response.data
      this.$cookies.set('selfishCrtId', cart_id, { domain: process.authDomain, path: '/' })
      commit('saveCartId', cart_id)
      commit('addProduct', products)
      await dispatch('peekProduct')
    } catch (error) {
      console.log(error)
    }
  },
  /*
    this action is called on inita app load from 'nuxtServerInit'
    this action will get a CartID cookie and get cartProducts accordingly
    if the Cookie is not avaialble it will generate a new CartID and save it in Cookie
  */
  async cartInitSync({ commit, dispatch }) {
    try {
      let cartId = this.$cookies.get('selfishCrtId')
      if (!cartId) {
        const response = await this.$externalApi.cartIdInit()
        cartId = response.data.cart_id
        this.$cookies.set('selfishCrtId', cartId, { domain: process.authDomain, path: '/' })
      }
      commit('saveCartId', cartId)
      await dispatch('peekProduct')
    } catch (error) {
      console.log('error from cart sync')
      console.log(error)
      console.log('error from cart sync')
    }
  },
  async peekProduct({ commit, state, dispatch }) {
    try {
      const response = await this.$externalApi.cartPeek({ cart_id: state.cartId })
      const detailResponse = await this.$externalApi.cartDetails({ cart_id: state.cartId })
      const { products } = response.data
      const detailData = detailResponse.data
      commit('addProduct', products)
      commit('onCartDetail', detailData)
    } catch (error) {
      console.log(error.response)
      this.$cookies.remove('selfishCrtId', { domain: process.authDomain, path: '/' })
      dispatch('cartInitSync')
    }
  },
  async getCartDetail({ commit, state, dispatch }, query = {}) {
    try {
      const response = await this.$externalApi.cartDetails(Object.assign(query, { cart_id: state.cartId }))
      const { data } = response
      commit('onCartDetail', data)
    } catch (error) {
      console.log(error.response)
      commit('onCartDetailError', error.response.data.error)
    }
  },
  async removeProduct({ commit, state, dispatch }, prodData) {
    try {
      await this.$externalApi.removeProduct(prodData, { cart_id: state.cartId })
      const response = await this.$externalApi.cartPeek({ cart_id: state.cartId })
      const detailResponse = await this.$externalApi.cartDetails({ cart_id: state.cartId })
      const { products } = response.data
      const detailData = detailResponse.data
      commit('addProduct', products)
      commit('onCartDetail', detailData)
    } catch (error) {
      commit('onCartDetailError', error.response.data.error)
    }
  },
  async getAddressList({ commit, state, dispatch }, addressList) {
    try {
      const response = await this.$externalApi.getAddressList()
      const { addresses } = response.data
      commit('onAddressList', addresses)
    } catch (error) {

    }
  },
  onAddressSelect({ commit, dispatch }, addressID) {
    commit('onAddressSelect', Number(addressID))
    dispatch('getCartDetail', { 'address_id': addressID })
  },
  async getOrderID({ commit, dispatch, state }) {
    try {
      const response = await this.$externalApi.getOrderID({ 'address_id': state.selectedAddress })
      commit('onOrderID', response.data.order_id)
    } catch (error) {
      console.log(error.response)
    }
  },
  async getPaymentModes({ commit, dispatch, state }) {
    try {
      const response = await this.$externalApi.getPaymentModes({ 'order_id': state.orderID })
      commit('onPaymentModes', response.data)
      console.log(response.data)
    } catch (error) {
      console.log(error.response)
    }
  }
}

export const getters = {
  bagsProductCount(state) {
    return state.bagProducts.length
  },
  getTotalPrice(state) {
    return state.cartDetails.find(priceObj => priceObj.type === 'total')
  },
  selectedAddObj(state) {
    if (state.selectedAddress) {
      return state.addressList.find(address => address.address_id === state.selectedAddress)
    }
    return {}
  },
  getAllWallet(state) {
    return (state.paymentMode && state.paymentMode.WALLET) ? state.paymentMode.WALLET.payment_methods : []
  },
  getPreferredBanks(state) {
    return (state.paymentMode && state.paymentMode.NB) ? state.paymentMode.NB.preferred_payment_methods : []
  },
  getAllBanks(state) {
    return (state.paymentMode && state.paymentMode.NB) ? [...state.paymentMode.NB.preferred_payment_methods, ...state.paymentMode.NB.payment_methods] : []
  }
}
