export const state = () => ({
  foreignId: null,
  foreignType: '',
  metaData: {}
})
export const mutations = {
  updatePageMeta(state, data) {
    state.foreignId = data.foreign_id
    state.foreignType = data.foreign_type
    state.metaData = data.meta_data
  }
}
export const actions = {
  updatePageMeta({ commit }, data) {
    commit('updatePageMeta', data)
  }
}
